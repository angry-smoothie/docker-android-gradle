# Introduction

- Android SDK version: 24.4.1
- Build tools version: 23.0.2
- Java version: Open JDK 7
- Gradle version: 2.11
- OS: Debian Jessie

# How to run it ?

Just mount your projects and change workdir accordingly.

Example:

```yaml
version: '2'
services:
  android-gradle:
    image: docker-android-gradle
    working_dir: ${CI_PROJECT_DIR}
    volumes:
      - ${CI_PROJECT_DIR}:${CI_PROJECT_DIR}
```