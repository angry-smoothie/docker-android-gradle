FROM java:7

MAINTAINER Alexis PIRES <pires.alexis@gmail.com>

# Install Deps for x86 compatibility
RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y --force-yes expect libc6-i386 lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1

# Install android
RUN cd /usr/local/ && \
    wget http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz && \
    tar -xvzf android-sdk_r24.4.1-linux.tgz && \
    chown -R root:root /usr/local/android-sdk-linux/ && \
    rm android-sdk_r24.4.1-linux.tgz

# Set android env
ENV ANDROID_HOME /usr/local/android-sdk-linux
ENV PATH $PATH:$ANDROID_HOME/tools
ENV PATH $PATH:$ANDROID_HOME/platform-tools

# Install latest android tools and system images
RUN echo "y" | android update sdk --filter platform --no-ui -a -s && \
    echo "y" | android update sdk --filter platform-tools --no-ui -a -s && \
    echo "y" | android update sdk --filter build-tools-23.0.2 --no-ui -a -s && \
    echo "y" | android update sdk --filter extra-android-m2repository --no-ui -a -s && \
    echo "y" | android update sdk --filter extra-google-m2repository --no-ui -a -s && \
    echo "y" | android update adb

# Set gradle env
ENV GRADLE_HOME=/opt/gradle \
    GRADLE_USER_HOME=/cache/.gradle
ENV PATH $PATH:$GRADLE_HOME/bin

# Install Gradle
RUN cd /opt && \
    curl -L https://services.gradle.org/distributions/gradle-2.11-bin.zip > gradle-2.11-bin.zip && \
    unzip gradle-2.11-bin.zip && \
    mv gradle-2.11 ${GRADLE_HOME} && \
    rm gradle-2.11-bin.zip
